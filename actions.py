import cv2
import numpy as np
from matplotlib.backends.backend_qtagg import FigureCanvasQTAgg as FigureCanvas
from PySide6.QtCharts import (QBarSeries, QBarSet, QCategoryAxis, QChart,
                              QChartView, QValueAxis)
from PySide6.QtCore import Qt
from PySide6.QtGui import QImage, QPixmap
from PySide6.QtWidgets import (QApplication, QFileDialog, QGraphicsPixmapItem,
                               QGraphicsScene)
from qt_material import apply_stylesheet
from window import Ui_MainWindow
from CustomWidgets.HistogramWindow import HistogramWindow
import tempfile
import os

kernel = np.ones((2, 2), np.uint8)

tempPath = os.path.join(os.path.dirname(__file__), 'temp')
if(os.path.exists(tempPath) == False):
    os.mkdir(tempPath)
tempfile.tempdir = tempPath
class Actions():
    mainWindows: Ui_MainWindow
    app: QApplication
    pixelMap: QPixmap
    modifiedImage: np.ndarray
    file_name: str
    img: np.ndarray

    def __init__(self, mainWindows: Ui_MainWindow, app: QApplication) -> None:
        self.mainWindows = mainWindows
        self.app = app
        self.mainWindows.imgGraphiqueView.setVisible(False)
        self.mainWindows.resetWidget.setVisible(False)
        self.file_name = ''
        self.mainWindows.actionsContainer.setEnabled(False)
        self.pixelMap = QPixmap()
        # clear temp folder on close
        app.aboutToQuit.connect(lambda: [os.remove(os.path.join(os.path.dirname(__file__), 'temp', f)) for f in os.listdir(tempPath)] if os.path.exists(tempPath) else None)

    def importImg(self):
        file_filter = "Images (*.jpg *.jpeg *.png *.bmp *.gif)"
        file_name = QFileDialog.getOpenFileName(
            None, "Open Image", "", file_filter)
        if (file_name[0] == ''):
            return
        self.file_name = file_name[0]
        self.img = cv2.imread(self.file_name)
        self.modifiedImage = cv2.imread(self.file_name)
        self.pixelMap = QPixmap(self.file_name)
        self.addImageDisplay()

    def addImageDisplay(self):
        if (self.pixelMap.isNull()):
            return
        item = QGraphicsPixmapItem(self.pixelMap)
        scene = QGraphicsScene()
        scene.addItem(item)

        self.mainWindows.imgGraphiqueView.setScene(scene)

        self.mainWindows.importWidget.setVisible(False)
        self.mainWindows.imgGraphiqueView.setVisible(True)
        self.mainWindows.resetWidget.setVisible(True)
        self.mainWindows.actionsContainer.setEnabled(True)

    def grayscale(self):
        if (self.file_name == ''):
            return
        gray = cv2.cvtColor(self.modifiedImage, cv2.COLOR_BGR2GRAY)
        q_image = QImage(gray.data, gray.shape[1], gray.shape[0], gray.strides[0], QImage.Format.Format_Grayscale8)
        self.pixelMap = QPixmap.fromImage(q_image)
        self.modifiedImage = gray
        self.addImageDisplay()

    def min(self):
        if (self.file_name == ''):
            return
        erosion = cv2.erode(self.modifiedImage, kernel, iterations=1)
        q_image = QImage(erosion.data, erosion.shape[1], erosion.shape[0], erosion.strides[0], QImage.Format.Format_RGB888)
        self.pixelMap = QPixmap.fromImage(q_image)
        self.modifiedImage = erosion
        self.addImageDisplay()

    def max(self):
        if (self.file_name == ''):
            return
        dilation = cv2.dilate(self.modifiedImage, kernel, iterations=1)
        q_image = QImage(dilation.data, dilation.shape[1], dilation.shape[0], dilation.strides[0], QImage.Format.Format_RGB888)
        self.pixelMap = QPixmap.fromImage(q_image)
        self.modifiedImage = dilation
        self.addImageDisplay()

    def Median(self):
        if (self.file_name == ''):
            return
        median = cv2.medianBlur(self.modifiedImage, 5)
        q_image = QImage(median.data, median.shape[1], median.shape[0], median.strides[0], QImage.Format.Format_RGB888)
        self.pixelMap = QPixmap.fromImage(q_image)
        self.modifiedImage = median
        self.addImageDisplay()

    def reset(self):
        self.pixelMap = QPixmap(self.file_name)
        self.modifiedImage = self.img
        self.addImageDisplay()

    def contrast(self, alpha=3.0, beta=0.0):
        if (self.file_name == ''):
            return
        contrast = cv2.convertScaleAbs(self.modifiedImage, alpha=alpha, beta=beta)
        q_image = QImage(contrast.data, contrast.shape[1], contrast.shape[0], contrast.strides[0], QImage.Format.Format_RGB888)
        self.pixelMap = QPixmap.fromImage(q_image)
        self.modifiedImage = contrast
        self.addImageDisplay()

    def edgeDetection(self):
        if (self.file_name == ''):
            return
        edges = cv2.Canny(self.modifiedImage, 100, 200)
        q_image = QImage(edges.data, edges.shape[1], edges.shape[0], edges.strides[0], QImage.Format.Format_Grayscale8)
        self.pixelMap = QPixmap.fromImage(q_image)
        self.modifiedImage = edges
        self.addImageDisplay()

    def exportImg(self):
        if (self.file_name == ''):
            return
        file_filter = "Images (*.jpg *.jpeg *.png *.bmp *.gif)"
        file_name = QFileDialog.getSaveFileName(
            None, "Save Image", "", file_filter)
        if (file_name[0] == ''):
            return
        cv2.imwrite(file_name[0], self.modifiedImage)
    
    def printImg(self):
        file = tempfile.mktemp(suffix=".png")
        img = cv2.cvtColor(self.modifiedImage, cv2.COLOR_BGR2RGB)
        cv2.imwrite(file, img)
        os.startfile(file, "print")
                
    def showHistogram(self):
        if self.file_name == '':
            return
        bgr_channels = cv2.split(self.modifiedImage)
        colors = ['#0000FF', '#00FF00', '#D70040']
        chart_views = []

        for i, (channel, color) in enumerate(zip(bgr_channels, colors)):
            chart_view = QChartView()
            chart = QChart()
            chart_view.setChart(chart)

            histogram_data = cv2.calcHist(
                [channel], [0], None, [256], [0, 256])
            series = QBarSeries()
            bar_set = QBarSet(f'Channel {i}')
            for j, value in enumerate(histogram_data.flatten()):
                bar_set.append(value)

            series.append(bar_set)
            chart.addSeries(series)
            if len(bgr_channels) == 1:
                chart.setTitle('Gray')
                bar_set.setColor(color)
            else:
                chart.setTitle(
                    f'Channel {i == 0 and "B" or i == 1 and "G" or "R"}')
                bar_set.setColor(color)
            chart.createDefaultAxes()
            chart.legend().setVisible(False)

            axis_x = QCategoryAxis()
            axis_x.setRange(0, 256)
            axis_x.setTickCount(9)
            axis_x.setTickInterval(32)
            axis_x.setLabelsVisible(True)

            axisX = QValueAxis()
            chart.addAxis(axisX, Qt.AlignmentFlag.AlignBottom)
            series.attachAxis(axisX)

            axisX.setRange(0, 256)
            axisX.setTickCount(11)
            axisX.setTickInterval(25)
            chart_views.append(chart_view)

        self.w = HistogramWindow(chart_views)
        screen = QApplication.primaryScreen()
        screen_size = screen.size()
        self.w.setGeometry(0, 0, screen_size.width(), screen_size.height())
        self.w.show()

    def showLuminosityDialog(self):
        self.mainWindows.dialog.setFilter("Luminosity", 0, 100)
        self.mainWindows.dialog.show()
        self.mainWindows.dialog.pushButton.clicked.connect(
            lambda: self.contrast(alpha=1.0, beta=self.mainWindows.dialog.horizontalSlider.value()))

    def showContrastDialog(self):
        self.mainWindows.dialog.setFilter("Contrast", 0, 10)
        self.mainWindows.dialog.show()
        self.mainWindows.dialog.pushButton.clicked.connect(
            lambda: self.contrast(alpha=self.mainWindows.dialog.horizontalSlider.value(), beta=0.0))
          
    def setupActions(self):
        self.mainWindows.importBtn.clicked.connect(self.importImg)

        # Image Actions
        self.mainWindows.greyShadow.clicked.connect(self.grayscale)
        self.mainWindows.Min.clicked.connect(self.min)
        self.mainWindows.Max.clicked.connect(self.max)
        self.mainWindows.Median.clicked.connect(self.Median)
        self.mainWindows.reset.clicked.connect(self.reset)
        self.mainWindows.print.clicked.connect(self.printImg)
        self.mainWindows.contrast.clicked.connect(self.showContrastDialog)
        self.mainWindows.lumninace.clicked.connect(self.showLuminosityDialog)
        self.mainWindows.actionDetect_Edges.triggered.connect(
            self.edgeDetection)

        self.mainWindows.actionHistogram.triggered.connect(self.showHistogram)
        # Actions
        self.mainWindows.actionOpen_file.triggered.connect(self.importImg)
        self.mainWindows.actionExit.triggered.connect(self.app.quit)
        self.mainWindows.actionExport.triggered.connect(self.exportImg)

        # Theme
        self.mainWindows.actionDark.triggered.connect(
            lambda: apply_stylesheet(self.app, theme='dark_blue.xml'))
        self.mainWindows.actionLight.triggered.connect(
            lambda: apply_stylesheet(self.app, theme='light_blue.xml'))
