# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'DialogFilter.ui'
##
## Created by: Qt User Interface Compiler version 6.7.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################
from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtWidgets import (QLabel, QHBoxLayout, QPushButton, QDialog,
    QSlider, QVBoxLayout, QWidget,QSpacerItem,QSizePolicy)

class Ui_Dialog(QDialog):
    
    def __init__(self, parent: QWidget | None ) -> None:
        super().__init__(parent)
        self.setupUi("Filter",0,255)

    def setFilter(self,label: str, min: int, max: int):
        self.label.setText(label)
        self.horizontalSlider.setMinimum(min)
        self.horizontalSlider.setMaximum(max)
        
                
    def setupUi(self,label: str, min: int, max: int):
        if not self.objectName():
            self.setObjectName(u"self")
        self.setFixedSize(240,100)
        self.verticalLayout = QVBoxLayout(self)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.mainLayout = QVBoxLayout()
        self.mainLayout.setObjectName(u"mainLayout")
        self.sliderLayout = QHBoxLayout()
        self.sliderLayout.setObjectName(u"sliderLayout")
        self.horizontalSlider = QSlider(self)
        self.horizontalSlider.setObjectName(u"horizontalSlider")
        self.horizontalSlider.setOrientation(Qt.Orientation.Horizontal)
        self.horizontalSlider.setTickPosition(QSlider.TickPosition.TicksBelow)
        self.horizontalSlider.setTickInterval(1)
        
        
        self.horizontalSlider.setSingleStep(1)
        self.horizontalSlider.setMinimum(min)
        self.horizontalSlider.setMaximum(max)
        
        self.label = QLabel(label)
        self.sliderValue = QLabel('0')

        self.horizontalSlider.valueChanged.connect(lambda value: self.sliderValue.setText(str(value)))
        
        self.sliderLayout.addWidget(self.label)
        self.sliderLayout.addWidget(self.horizontalSlider)
        self.sliderLayout.addWidget(self.sliderValue)
        
        self.mainLayout.addLayout(self.sliderLayout)
        
        self.btnLayout = QHBoxLayout()
        self.btnLayout.setObjectName(u"btnLayout")
        self.pushButton = QPushButton(self)
        self.pushButton.setObjectName(u"pushButton")

        self.btnLayout.addWidget(self.pushButton)

        self.pushButton_2 = QPushButton(self)
        self.pushButton_2.setObjectName(u"pushButton_2")

        self.btnLayout.addWidget(self.pushButton_2)


        self.mainLayout.addLayout(self.btnLayout)

        self.mainLayout.setStretch(0, 2)
        self.mainLayout.setStretch(1, 1)

        self.verticalLayout.addLayout(self.mainLayout)


        self.retranslateUi()

        QMetaObject.connectSlotsByName(self)
        
        self.pushButton_2.clicked.connect(self.close)
        
    # setupUi

    def retranslateUi(self):
        self.setWindowTitle(QCoreApplication.translate("Filter", u"Filter", None))
        self.pushButton.setText(QCoreApplication.translate("Filter", u"Apply", None))
        self.pushButton_2.setText(QCoreApplication.translate("Filter", u"cancel", None))
    # retranslateUi
