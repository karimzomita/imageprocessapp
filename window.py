# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'Pj_Img.ui'
##
## Created by: Qt User Interface Compiler version 6.7.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
####################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
                            QMetaObject, QObject, QPoint, QRect, QSize, Qt,
                            QTime, QUrl)
from PySide6.QtGui import (QAction, QBrush, QColor, QConicalGradient, QCursor,
                           QFont, QFontDatabase, QGradient, QIcon, QImage,
                           QKeySequence, QLinearGradient, QPainter, QPalette,
                           QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QGraphicsView, QHBoxLayout,
                               QMainWindow, QMenu, QMenuBar, QPushButton,
                               QSizePolicy, QSpacerItem, QVBoxLayout, QWidget, QLabel)
from CustomWidgets.ImageViewer import ImageViewer
from DialogFilter import Ui_Dialog

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        self.dialog = Ui_Dialog(MainWindow)
        MainWindow.resize(800, 600)
        self.actionOpen_file = QAction(MainWindow)
        self.actionOpen_file.setObjectName(u"actionOpen_file")
        self.actionExport = QAction(MainWindow)
        self.actionExport.setObjectName(u"actionExport")
        self.actionExit = QAction(MainWindow)
        self.actionExit.setObjectName(u"actionExit")
        self.actionDetect_Edges = QAction(MainWindow)
        self.actionDetect_Edges.setObjectName(u"actionDetect_Edges")
        self.actionHistogram = QAction(MainWindow)
        self.actionHistogram.setObjectName(u"actionHistogram")
        self.actionDark = QAction(MainWindow)
        self.actionDark.setObjectName(u"actionDark")
        self.actionLight = QAction(MainWindow)
        self.actionLight.setObjectName(u"actionLight")
        self.actionAbout = QAction(MainWindow)
        self.actionAbout.setObjectName(u"actionAbout")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.VMLayout = QVBoxLayout(self.centralwidget)
        self.VMLayout.setSpacing(0)
        self.VMLayout.setObjectName(u"VMLayout")
        self.VMLayout.setContentsMargins(0, 0, 0, 0)
        self.mainContainer = QHBoxLayout()
        self.mainContainer.setObjectName(u"mainContainer")
        self.actionsContainer = QWidget(self.centralwidget)
        self.actionsContainer.setObjectName(u"actionsContainer")
        self.actionsContainer.setEnabled(True)
        self.verticalLayout = QVBoxLayout(self.actionsContainer)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.lumninace = QPushButton(self.actionsContainer)
        self.lumninace.setObjectName(u"lumninace")

        self.verticalLayout.addWidget(self.lumninace)

        self.contrast = QPushButton(self.actionsContainer)
        self.contrast.setObjectName(u"contrast")

        self.verticalLayout.addWidget(self.contrast)

        self.greyShadow = QPushButton(self.actionsContainer)
        self.greyShadow.setObjectName(u"greyShadow")

        self.verticalLayout.addWidget(self.greyShadow)

        self.Min = QPushButton(self.actionsContainer)
        self.Min.setObjectName(u"Min")

        self.verticalLayout.addWidget(self.Min)

        self.Median = QPushButton(self.actionsContainer)
        self.Median.setObjectName(u"Median")

        self.verticalLayout.addWidget(self.Median)

        self.Max = QPushButton(self.actionsContainer)
        self.Max.setObjectName(u"Max")

        self.verticalLayout.addWidget(self.Max)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer)


        self.mainContainer.addWidget(self.actionsContainer)

        self.imageContainer = QVBoxLayout()
        self.imageContainer.setObjectName(u"imageContainer")
        self.importWidget = QWidget(self.centralwidget)
        self.importWidget.setObjectName(u"importWidget")
        self.horizontalLayout_2 = QHBoxLayout(self.importWidget)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer)

        self.importBtn = QPushButton(self.importWidget)
        self.importBtn.setObjectName(u"importBtn")

        self.horizontalLayout_2.addWidget(self.importBtn)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer_2)


        self.imageContainer.addWidget(self.importWidget)

        self.imgGraphiqueView = ImageViewer(self.centralwidget)
        self.imgGraphiqueView.setObjectName(u"imgGraphiqueView")

        self.imageContainer.addWidget(self.imgGraphiqueView)

        self.resetWidget = QWidget(self.centralwidget)
        self.resetWidget.setObjectName(u"resetWidget")
        self.horizontalLayout_3 = QHBoxLayout(self.resetWidget)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalSpacer_3 = QSpacerItem(40, 20, QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer_3)

        self.reset = QPushButton(self.resetWidget)
        self.reset.setObjectName(u"reset")
        self.print = QPushButton(self.resetWidget)
        self.print.setObjectName(u"print")

        self.horizontalLayout_3.addWidget(self.reset)
        self.horizontalLayout_3.addWidget(self.print)

        self.imageContainer.addWidget(self.resetWidget)


        self.mainContainer.addLayout(self.imageContainer)

        self.mainContainer.setStretch(0, 1)
        self.mainContainer.setStretch(1, 5)

        self.VMLayout.addLayout(self.mainContainer)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 839, 33))
        self.menuFile = QMenu(self.menubar)
        self.menuFile.setObjectName(u"menuFile")
        self.menuImage = QMenu(self.menubar)
        self.menuImage.setObjectName(u"menuImage")
        self.menuMore = QMenu(self.menubar)
        self.menuMore.setObjectName(u"menuMore")
        self.menuTheme = QMenu(self.menuMore)
        self.menuTheme.setObjectName(u"menuTheme")
        MainWindow.setMenuBar(self.menubar)

        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuImage.menuAction())
        self.menubar.addAction(self.menuMore.menuAction())
        self.menuFile.addAction(self.actionOpen_file)
        self.menuFile.addAction(self.actionExport)
        self.menuFile.addSeparator()
        self.menuFile.addAction(self.actionExit)
        self.menuImage.addAction(self.actionDetect_Edges)
        self.menuImage.addAction(self.actionHistogram)
        self.menuMore.addAction(self.menuTheme.menuAction())
        self.menuMore.addAction(self.actionAbout)
        self.menuTheme.addAction(self.actionDark)
        self.menuTheme.addAction(self.actionLight)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"Image Modif", None))
        self.actionOpen_file.setText(QCoreApplication.translate("MainWindow", u"Open file", None))
        self.actionExport.setText(QCoreApplication.translate("MainWindow", u"Export", None))
        self.actionExit.setText(QCoreApplication.translate("MainWindow", u"Exit", None))
        self.actionDetect_Edges.setText(QCoreApplication.translate("MainWindow", u"Detect Edges", None))
        self.actionHistogram.setText(QCoreApplication.translate("MainWindow", u"Histogram", None))
        self.actionDark.setText(QCoreApplication.translate("MainWindow", u"Dark", None))
        self.actionLight.setText(QCoreApplication.translate("MainWindow", u"Light", None))
        self.actionAbout.setText(QCoreApplication.translate("MainWindow", u"About", None))
        self.lumninace.setText(QCoreApplication.translate("MainWindow", u"Luminosity", None))
        self.contrast.setText(QCoreApplication.translate("MainWindow", u"Contrast", None))
        self.greyShadow.setText(QCoreApplication.translate("MainWindow", u"Grey Shadow", None))
        self.Min.setText(QCoreApplication.translate("MainWindow", u"Min", None))
        self.Median.setText(QCoreApplication.translate("MainWindow", u"Median", None))
        self.Max.setText(QCoreApplication.translate("MainWindow", u"Max", None))
        self.importBtn.setText(QCoreApplication.translate("MainWindow", u"Import", None))
        self.reset.setText(QCoreApplication.translate("MainWindow", u"Reset", None))
        self.print.setText(QCoreApplication.translate("MainWindow", u"Print", None))
        self.menuFile.setTitle(QCoreApplication.translate("MainWindow", u"File", None))
        self.menuImage.setTitle(QCoreApplication.translate("MainWindow", u"Image", None))
        self.menuMore.setTitle(QCoreApplication.translate("MainWindow", u"More", None))
        self.menuTheme.setTitle(QCoreApplication.translate("MainWindow", u"Theme", None))
    # retranslateUi


