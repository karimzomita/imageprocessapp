from PySide6.QtWidgets import QVBoxLayout, QWidget

class HistogramWindow(QWidget):
    def __init__(self, chart_views):
        super().__init__()
        layout = QVBoxLayout()
        for chart_view in chart_views:
            layout.addWidget(chart_view)
        self.setLayout(layout)
        self.setWindowTitle("Histogram")
