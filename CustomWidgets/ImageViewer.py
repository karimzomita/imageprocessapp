import sys

from PySide6.QtCore import Qt
from PySide6.QtGui import QPixmap, QWheelEvent
from PySide6.QtWidgets import (QApplication, QGraphicsPixmapItem,
                               QGraphicsScene, QGraphicsView, QLabel,
                               QMainWindow, QScrollArea)


class ImageViewer(QGraphicsView):
    def __init__(self,parent=None):
        super().__init__(parent)
        self.setTransformationAnchor(self.ViewportAnchor.AnchorUnderMouse)

    def wheelEvent(self, event):
        if event.modifiers() :
            angle = event.angleDelta().y()
            zoomFactor = 1 + (angle/1000)
            self.scale(zoomFactor, zoomFactor)
        else:
            super().wheelEvent(event)