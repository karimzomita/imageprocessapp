import sys

from actions import Actions
from window import QApplication, QMainWindow, Ui_MainWindow

from qt_material import apply_stylesheet

if __name__ == '__main__':
    app = QApplication(sys.argv)
    MainWindow = QMainWindow()
    MainWindow.setObjectName("Image Editor")
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    
    apply_stylesheet(app, theme='dark_blue.xml',extra={
       'density_scale': -2,
        'pyside6': True
    })
    
    actions = Actions(ui,app)
    actions.setupActions()
    
    MainWindow.show()
    sys.exit(app.exec())